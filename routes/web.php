<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/logout', function() {
	Auth::logout();
});

Route::get('/home', ['as' => 'home' ,'uses' => 'HomeController@index']);

Route::get('/{id}/images', ['as' => 'user.images', 'uses' => 'HomeController@images']);
Route::get('/image/{id}/delete', ['as' => 'image.delete', 'uses' => 'HomeController@destroy']);
Route::get('/image/{id}/edit', ['as' => 'image.edit', 'uses' => 'HomeController@edit']);
Route::post('/image/{id}/edit', ['as' => 'image.update', 'uses' => 'HomeController@update']);
