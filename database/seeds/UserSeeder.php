<?php

use Illuminate\Database\Seeder;

use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
        	'name' 		=> 'Admin',
        	'email' 	=> 'admin@admin.com',
        	'dni' 		=> '12345678',
        	'password'	=> bcrypt('password123'),
        	'api_token'	=> str_random(60)
        ]);
    }
}
