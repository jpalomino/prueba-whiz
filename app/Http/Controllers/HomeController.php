<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\ImageUploadRequest;
use App\Http\Requests\ImageEditRequest;

use App\User;
use App\Image;

use Exception;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        #$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('user.index', ['users' => $users]);
    }

    public function store(UserCreateRequest $request)
    {
        $data = $request->only(['name', 'dni', 'email']);

        $data['api_token']  = str_random(60);
        $data['password']   = bcrypt(str_random(60));

        $user = User::create($data);

        return $user;
    }

    public function images($id)
    {
        $images = User::findOrFail($id)->images;

        return view('user.images', ['images' => $images]);
    }

    public function destroy($id)
    {
        $image = Image::findOrFail($id);
        $image->delete();

        return redirect()->back();
    }

    public function edit($id)
    {
        return view('user.edit', ['id' => $id]);
    }

    public function update($id, ImageEditRequest $request)
    {
        $image = Image::findOrFail($id);
        \Storage::delete($image->path);
        $image->path = $request->image->store('images');
        $image->save();

        return redirect()->route('user.images', $image->user->id);
    }

    public function upload(ImageUploadRequest $request)
    {
        try{
            $user = User::where('api_token',$request->header('Authorization'))->first();

            if(!$user) {
                return response()->json(['status' => 'error', 'message' => 'Unauthorized user', 'hint' => 'check your token'], 401);
            }

            $image = new Image;

            $image->path = $request->image->store('images'); 

            $user->images()->save($image);

            return response()->json(['status' => 'success', 'message' => 'OK']);
        } catch (Exception $e){
        return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }
    }
}
