@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th>Image</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </thead>
                        <tbody>
                            @foreach($images as $image)
                                <tr>
                                    <td> <img src="{{ asset($image->path) }}" width="300"> </td>
                                    <td> <a href="{{ route('image.edit', $image->id) }}">Edit</a> </td>
                                    <td> <a href="{{ route('image.delete', $image->id) }}">Delete</a> </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection